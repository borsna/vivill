<?php

namespace Drupal\vivill\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Yaml\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Console\Core\Command\ContainerAwareCommand;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\vivill\Service\DocumentService;

/**
 * Class ImportCommand.
 *
 * @DrupalCommand (
 *     extension="vivill",
 *     extensionType="module"
 * )
 */
class ImportCommand extends ContainerAwareCommand{

  /**
   * {@inheritdoc}
   */
  public function __construct(DocumentService $document_service, ModuleHandlerInterface $module_handler) {
    $this->document_service = $document_service;
    $this->module_handler = $module_handler;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('vivill:import')
      ->setDescription($this->trans('commands.vivill.import.description'))
      ->addArgument(
        'path',
        InputArgument::REQUIRED,
        $this->trans('commands.vivill.import.arguments.path'),
        null
    );;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $path = $input->getArgument('path');

    $documents = $this->getDocumentList($path);

    $this->document_service->createIndex();
    
    $progress = new ProgressBar($output, count($documents));
    $progress->start();

    foreach($documents as $i => $document){
      $name_parts = explode('-', $document->name);
      $result = $this->document_service->indexDocument($document->name, [
        'party' => $name_parts[0],
        'year' => $name_parts[1],
        'type' => $name_parts[2],
        'text' => file_get_contents($document->uri)
      ]);
      $progress->advance();
    }
    
    $progress->finish();

    $io->info($this->trans('commands.vivill.import.messages.success'));
  }

  private function getDocumentList(string $path){
    $document_regexp = '/[A-z]{1,5}\-\d{4}\-[a-z].txt/';
    return file_scan_directory($path, $document_regexp, $options = [], $depth = 0);
  }

  private function getPartyArray(){
    $module_path = $this->module_handler->getModule('vivill')->getPath();
    $yaml_path = $module_path.'/resources/documents/parties.yaml';

    return Yaml::parse(file_get_contents($yaml_path));
  }

}
