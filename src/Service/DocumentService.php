<?php
namespace Drupal\vivill\Service;

use Elasticsearch\ClientBuilder;

class DocumentService {
  protected $client;
  protected $config;

  public function __construct() {
    $this->config = \Drupal::config('vivill.settings');

    $hosts = [
      $this->config->get('vivill-elasticsearch-host')
    ];

    $this->client = ClientBuilder::create()
                                   ->setHosts($hosts)
                                   ->build();
  }

  public function getDefaultParams(){
    return [
      'index' => 'vivill',
      'type' => 'document'
    ];
  }

  public function indexDocument(string $id, array $document){
    $params = $this->getDefaultParams();
    $params['id'] = $id;
    $params['body'] = $document;
    $response = $this->client->index($params);
    return $response;
  }

  public function createIndex(){
    try{
      $this->client->indices()->create([
        'index' => 'vivill',
        'body' => $this->mapping()
      ]);
    }catch(\Elasticsearch\Common\Exceptions\BadRequest400Exception $e){
      $this->client->indices()->delete(['index' => 'vivill']);
      $this->client->indices()->create([
        'index' => 'vivill',
        'body' => $this->mapping()
      ]);
    }
  }

  public function get(string $document_slug) {
    $params = $this->getDefaultParams();
    $params['id'] = $document_slug;

    try{
      $response = $this->client->get($params);
    } catch (Exception $e) {
      if ($this->client->indices()->exists(['index' => $params['index']])) {
        drupal_set_message('Error geting document "' . json_encode($params, JSON_PRETTY_PRINT) . '"', 'error');
        drupal_set_message($e, 'error');
      } else {
        drupal_set_message('Index "' . $index . '" does not exist, run drush simp', 'error');
      }
    }
    return $response['_source'];
  }

  public function search(string $string = null, array $arguments = null){
    $params = $this->getDefaultParams();
    $params['body'] = [
      'query' => [
        'query_string' => [
          'query' => $string
        ]
      ],
      'aggs' => $this->aggregations()
    ];
    $response = $this->client->search($params);

    return $response;
  }

  private function mapping(){
    return [
      'settings' => [
        'number_of_shards' => 3,
        'number_of_replicas' => 2
      ],
      'mappings' => [
          'document' => [
              '_source' => [
                'enabled' => true
              ],
              'properties' => [
                  'year' => [
                    'type' => 'date',
                    'format' => 'yyyy'
                  ],
                  'type' => [
                    'type' => 'keyword'
                  ],
                  'party' => [
                    'type' => 'keyword'
                  ],
                  'text' => [
                    'type' => 'text',
                    'analyzer' => 'swedish'
                  ]
              ]
          ]
      ]
    ];
  }

  private function aggregations(){
    return [
      'party' => [
        'terms' => [
          'field' => 'party'
        ]
      ],
      'year' => [
        'histogram' => [
          'field' => 'year',
          'interval' => 1
        ]
      ],
    ];
  }

}