<?php

namespace Drupal\vivill\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

class SearchForm extends FormBase {
  
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'vivill_search_form';
  }
  
  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $query = \Drupal::request()->query;

    $form['search_text'] = [
      '#type' => 'textfield',
      '#attributes' => ['placeholder' => t('Search in SND\'s Catalogue')],
      '#maxlength' => 255,
      '#size' => 50,
      '#default_value' => $path_args[1],
      '#required' => FALSE,
    ];

    $form['search_submit'] = [
      '#type' => 'submit',
      '#value' => t('Search'),
      '#attributes' => ['class' => ['ui-button', 'ui-widget', 'ui-corner-all']]
    ];
    
    $form['search_uri'] = [
      '#type' => 'hidden',
      '#value' => UrlHelper::buildQuery(UrlHelper::filterQueryParameters($query->all()))
    ];
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $params = array();
    $search = $form_state['values']['search_text'];
    parse_str($form_state['values']['search_uri'], $params);
    
    //remove preceding wildcards
    $cleaned_search = removePrecedingWildcards(trim($search));

    $redirect_path = 'vivill/';

    drupal_goto($redirect_path . $cleaned_search, ['query' => $params]);
  }

  /**
  * Removes preceding wildcards (.*?~) of a query
  * @param string $query query to clean
  * @return string cleaned query
  */
  function removePrecedingWildcards($query){
    return preg_replace('/^([\.\*\?~]+)(?=[\wåäöÅÄÖ])+/', '', $query);    
  }
}