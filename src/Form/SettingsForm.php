<?php

namespace Drupal\vivill\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure vivill settings for this site.
 */
class SettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vivill_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vivill.settings',
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vivill.settings');

    $form['elasticsearch-host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Elasticsearch host'),
      '#default_value' => $config->get('vivill.elasticsearch-host'),
    ];

    $form['elasticsearch-aggreations-max'] = [
      '#type' => 'number',
      '#title' => $this->t('Elasticsearch aggreations max number to get'),
      '#default_value' => $config->get('vivill.elasticsearch-aggreations-max'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      
    \Drupal::configFactory()->getEditable('vivill.settings')
      ->set('vivill.elasticsearch-host', $form_state->getValue('elasticsearch-host'))
      ->set('vivill.elasticsearch-aggreations-max', $form_state->getValue('elasticsearch-aggreations-max'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}