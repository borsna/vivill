<?php
/**
 * @file
 * Contains \Drupal\vivill\Controller\IndexController.
 */
 
namespace Drupal\vivill\Controller;
 
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\vivill\Service\DocumentService;
 
class IndexController extends ControllerBase implements ContainerInjectionInterface{
  protected $document_service;

  public function __construct(DocumentService $document_service){
    $this->document_service = $document_service;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vivill.document_service')
    );
  }

  public function index() {
    $form = \Drupal::formBuilder()->getForm('Drupal\vivill\Form\SearchForm');
    
    return array(
      '#theme' => 'vivill_search',
      '#form' => $form,
      '#results' => ['apa' => 23],
      '#attached' => ['library' => ['vivill/vivill.theme']]
    );
  }
}